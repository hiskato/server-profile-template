#!/bin/sh -x 

create(){
    isctl apply -f $1
}
delete(){
  isctl apply --delete -f $1
}

create_all(){
create "compute_temp.yaml"
create "management_temp.yaml" 
create "network_temp.yaml"
create "san_temp.yaml"
create "pool_temp.yaml"
}

delete_all(){
delete "compute_temp.yaml"
delete "management_temp.yaml" 
delete "network_temp.yaml"
delete "san_temp.yaml"
delete "pool_temp.yaml"
}

if [ "$#" = "1" ];then
  if [ $1 = "create" ];then
     create_all
  fi
  if [ $1 = "delete" ];then
     delete_all
  fi
else
 echo "Usage: ./gen.sh create or ./gen.sh delete"
 exit 1
fi
  