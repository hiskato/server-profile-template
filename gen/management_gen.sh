#!/bin/sh 

gen(){
    ytt -f $1 -f $2
}
# Parameter FILE
PARAM="../values/management_values.yaml"
#Output file
OUTPUT="../output/management_temp.yaml"

# imc_access_policy
IMC_POL_FILE="../template/imc-access-pol-template.yaml"
OUTPUT_FILE="../manage/management/imc-access-pol.yaml"
gen $IMC_POL_FILE $PARAM > $OUTPUT
gen $IMC_POL_FILE $PARAM > $OUTPUT_FILE
echo "---" >> $OUTPUT

# kvm_policy
KVM_POL_FILE="../template/kvm-pol-template.yaml"
OUTPUT_FILE="../manage/management/kvm-pol.yaml"

gen $KVM_POL_FILE $PARAM >> $OUTPUT
gen $KVM_POL_FILE $PARAM > $OUTPUT_FILE
echo "---" >> $OUTPUT

# ipmi_policy
IPMI_POL_FILE="../template/ipmi-pol-template.yaml"
OUTPUT_FILE="../manage/management/ipmi-pol.yaml"
gen $IPMI_POL_FILE $PARAM >> $OUTPUT
gen $IPMI_POL_FILE $PARAM > $OUTPUT_FILE
echo "---" >> $OUTPUT

#vmedia_policy
VMEDIA_POL_FILE="../template/vmedia-pol-template.yaml"
OUTPUT_FILE="../manage/management/vmedia-pol.yaml"
gen $VMEDIA_POL_FILE $PARAM >> $OUTPUT
gen $VMEDIA_POL_FILE $PARAM > $OUTPUT_FILE
echo "---" >> $OUTPUT

#power_policy
POWER_POL_FILE="../template/power-pol-template.yaml"
OUTPUT_FILE="../manage/management/power-pol.yaml"
gen $POWER_POL_FILE $PARAM >> $OUTPUT
gen $POWER_POL_FILE $PARAM > $OUTPUT_FILE
echo "---" >> $OUTPUT

# Serial Over LAN Policy
SOL_POL_FILE="../template/sol-pol-template.yaml"
OUTPUT_FILE="../manage/management/sol-pol.yaml"
gen $SOL_POL_FILE $PARAM >> $OUTPUT
gen $SOL_POL_FILE $PARAM > $OUTPUT_FILE
echo "---" >> $OUTPUT

# Firmware Policy
FIRMWARE_POL_FILE="../template/firmware-pol-template.yaml"
OUTPUT_FILE="../manage/management/firmare-pol.yaml"
gen $FIRMWARE_POL_FILE $PARAM >> $OUTPUT
gen $FIRMWARE_POL_FILE $PARAM > $OUTPUT_FILE
echo "---" >> $OUTPUT

# local user policy
USER_POL_FILE="../template/localuser-pol-template.yaml"
OUTPUT_FILE="../manage/management/localuser-pol.yaml"
gen $USER_POL_FILE $PARAM >> $OUTPUT
gen $USER_POL_FILE $PARAM > $OUTPUT_FILE