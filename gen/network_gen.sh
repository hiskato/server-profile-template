#!/bin/sh
gen(){
    ytt -f $1 -f $2
}
# Parameter FILE
PARAM="../values/network_values.yaml"
#Output file
OUTPUT="../output/network_temp.yaml"
# LAN Connection Policy
LANC_FILE="../template/lancoonection-pol-template.yaml"
OUTPUT_FILE="../manage/network/lancoonection-pol.yaml"
gen $LANC_FILE $PARAM > $OUTPUT
gen $LANC_FILE $PARAM > $OUTPUT_FILE
echo "---" >> $OUTPUT
# EthNetworkControlPolicy
ETHCON_FILE="../template/networkcontrol-pol-template.yaml"
OUTPUT_FILE="../manage/network/networkcontrol-pol.yaml"
gen $ETHCON_FILE $PARAM >> $OUTPUT
gen $ETHCON_FILE $PARAM > $OUTPUT_FILE
echo "---"  >> $OUTPUT
# EthQosPolicy
NETQOS_FILE="../template/qos-pol-template.yaml"
OUTPUT_FILE="../manage/network/qos-pol.yaml"
gen $NETQOS_FILE $PARAM >> $OUTPUT
gen $NETQOS_FILE $PARAM > $OUTPUT_FILE
echo "---"  >> $OUTPUT
# EthNetwork Group Policy
NETG_FILE="../template/netgroup-pol-template.yaml"
OUTPUT_FILE="../manage/network/netgroup-pol.yaml"
gen $NETG_FILE $PARAM >> $OUTPUT
gen $NETG_FILE $PARAM > $OUTPUT_FILE

echo "---"  >> $OUTPUT
# Ethernet Adapter Policy
ETHA_FILE="../template/ehteradapter-pol-template.yaml"
OUTPUT_FILE="../manage/network/ehteradapter-pol.yaml"
gen $ETHA_FILE $PARAM >> $OUTPUT
gen $ETHA_FILE $PARAM > $OUTPUT_FILE

echo "---"  >> $OUTPUT
# Vnic Policy
VNIC_FILE="../template/vnic-pol-template.yaml"
OUTPUT_FILE="../manage/network/vnic-pol.yaml"
gen $VNIC_FILE $PARAM >> $OUTPUT
gen $VNIC_FILE $PARAM > $OUTPUT_FILE