#!/bin/sh 

gen(){
    ytt -f $1 -f $2
}

# Parameter FILE
PARAM="../values/compute_values.yaml"
#Output file
OUTPUT="../output/compute_temp.yaml"
# PrecisionPolicy
BOOTORDER_FILE="../template/boot_precisionpolicy_template.yaml"
OUTPUT_FILE="../manage/compute/boot_precisionpolicy.yaml"
gen $BOOTORDER_FILE $PARAM > $OUTPUT
gen $BOOTORDER_FILE $PARAM > $OUTPUT_FILE
echo "---" >> $OUTPUT
# BIOS
BIOS_FILE="../template/bios-policy-template.yaml"
OUTPUT_FILE="../manage/compute/bios-policy.yaml"
gen $BIOS_FILE $PARAM >> $OUTPUT
gen $BIOS_FILE $PARAM > $OUTPUT_FILE
