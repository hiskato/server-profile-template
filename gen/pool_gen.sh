#!/bin/sh 

gen(){
    ytt -f $1 -f $2
}
# Parameter FILE
PARAM="../values/pool_values.yaml"
# MANAGE FILE
OUTPUT="../output/pool_temp.yaml"
# uuidpool
UUIDPOOL_FILE="../template/uuid-pool-template.yaml"
OUTPUT_FILE="../manage/pool/uuid-pool.yaml"
gen $UUIDPOOL_FILE $PARAM > $OUTPUT
gen $UUIDPOOL_FILE $PARAM > $OUTPUT_FILE
echo "---" >> $OUTPUT

#macpool
MACPOOL_FILE="../template/macpool-template.yaml"
OUTPUT_FILE="../manage/pool/macpool.yaml"
gen $MACPOOL_FILE $PARAM >> $OUTPUT
gen $MACPOOL_FILE $PARAM > $OUTPUT_FILE
echo "---" >> $OUTPUT

#ipool
IPPOOL_FILE="../template/ippool-template.yaml"
OUTPUT_FILE="../manage/pool/ippool.yaml"
gen $IPPOOL_FILE $PARAM >> $OUTPUT
gen $IPPOOL_FILE $PARAM > $OUTPUT_FILE
echo "---" >> $OUTPUT

#fc pool
FCPOOL_FILE="../template/fcpool-template.yaml"
OUTPUT_FILE="../manage/pool/fcpool.yaml"
gen $FCPOOL_FILE $PARAM >> $OUTPUT
gen $FCPOOL_FILE $PARAM > $OUTPUT_FILE

