#!/bin/sh

gen(){
    ytt -f $1 -f $2
}
# Parameter FILE
PARAM="../values/san_values.yaml"
#Output file
OUTPUT="../output/san_temp.yaml"

# SanConnectivityPolicy
SANC_FILE="../template/sanconnection-pol-template.yaml"
OUTPUT_FILE="../manage/san/sanconnection-pol.yaml"
gen $SANC_FILE $PARAM > $OUTPUT
gen $SANC_FILE $PARAM > $OUTPUT_FILE
echo "---" >> $OUTPUT
#FcNetworkPolicy
FCN_FILE="../template/fcnet-pol-template.yaml"
OUTPUT_FILE="../manage/san/fcnet-pol.yaml"
gen $FCN_FILE $PARAM >> $OUTPUT
gen $FCN_FILE $PARAM > $OUTPUT_FILE
echo "---" >> $OUTPUT
#FcAdapterPolicy
FCA_FILE="../template/fcadapter-pol.yaml"
OUTPUT_FILE="../manage/san/fcadapter-pol.yaml"
gen $FCA_FILE $PARAM >> $OUTPUT
gen $FCA_FILE $PARAM > $OUTPUT_FILE
echo "---" >> $OUTPUT
#FcQosPolicy
FCQOS_FILE="../template/fcqos-pol-template.yaml"
OUTPUT_FILE="../manage/san/fcqos-pol.yaml"
gen $FCQOS_FILE $PARAM >> $OUTPUT
gen $FCQOS_FILE $PARAM > $OUTPUT_FILE
echo "---" >> $OUTPUT
#VHBA Policy
VHBA_FILE="../template/vhba_policy_template.yaml"
OUTPUT_FILE="../manage/san/vhba-policy.yaml"
gen $VHBA_FILE $PARAM >> $OUTPUT
gen $VHBA_FILE $PARAM > $OUTPUT_FILE